from fastapi import FastAPI,Request 

import json
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.responses import HTMLResponse


app = FastAPI()
templates = Jinja2Templates(directory="templates")
templates2 = Jinja2Templates(directory="templates2")
templates3 = Jinja2Templates(directory="templates3")
datos = {}
cels = {}
aptos = {}


@app.get("/")
def datas(request:Request):
    sin_data = json.dumps(datos)
    json_datos = json.loads(sin_data)
    return templates.TemplateResponse("inicio.html",{"request":request,"listado":json_datos})


@app.post("/agregar")
async def agregar(request:Request):
    nuevos_datos = {}
    formdata = await request.form()
    i = 1
    for id in datos:
        nuevos_datos[str(id)] = datos[id]
        i+=1
    datos[str(i)] = formdata["nuevolenguaje"]
    sin_data = json.dumps(datos)
    json.loads(sin_data) 
    return RedirectResponse("/",303)

@app.get("/")
def celu(request:Request):
    sin_celus = json.dumps(cels)
    json_cels = json.loads(sin_celus)
    return templates2.TemplateResponse("inicio.html",{"request":request,"listado":json_cels})


@app.post("/agregarcel")
async def agregarcel(request:Request):
    nuevos_cels = {}
    formdata = await request.form()
    e = 1
    for ed in cels:
        nuevos_cels[str(ed)] = cels[ed]
        e+=1
    cels[str(e)] = formdata["nuevocel"]
    sin_celus = json.dumps(cels)
    json.loads(sin_celus) 
    return RedirectResponse("/",303)

@app.get("/")
def apt(request:Request):
    sin_apts = json.dumps(aptos)
    json_apts = json.loads(sin_apts)
    return templates3.TemplateResponse("inicio.html",{"request":request,"listado":json_apts})

@app.post("/agregarapto")
async def agregarapto(request:Request):
    nuevos_aptos = {}
    formdata = await request.form()
    o = 1
    for od in aptos:
        nuevos_aptos[str(od)] = aptos[od]
        o+=1
    aptos[str(i)] = formdata["nuevoapto"]
    sin_apts = json.dumps(aptos)
    json.loads(sin_apts) 
    return RedirectResponse("/",303)

@app.get("/eliminar/{id}")
async def eliminar(request:Request,id,ed,od:str):
    del datos[id]
    del cels[ed]
    del aptos[od]
    return RedirectResponse("/",303)